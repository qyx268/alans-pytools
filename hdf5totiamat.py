#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Routine for converting Gadget3 HDF5 SubFind output to TIAMAT's binary format."""

import pandas as pd
import numpy as np
import numexpr as ne
import h5py
from calculateradius import *
from calculatediff import *
from pyread_gadget_hdf5 import *
from pyread_header_hdf5 import *

import fnmatch
import struct
import os
import sys
from collections import OrderedDict

from numpy import uint8, uint32, uint64, float32

__author__ = 'Alan Duffy'
__email__ = 'mail@alanrduffy.com'
__version__ = '0.1.0'


def subfindtotiamat(filename, longlong=None, noloop=None, silent=None, reslimit=None):
    """ Convert the SubFind Gadget3 HDF5 output to binary format for TIAMAT's binary format."""

    """
    +
     NAME:
            SUBFINDTOTIAMAT
    
     PURPOSE:
            This function converts HDF5 SubFind outputs of the OWLS Gadget3 code to Binary TIAMAT format

     CATEGORY:
            I/O, HDF5, TIAMAT / PAISTE

     REQUIREMENTS:
            import numpy as np
            import h5py
            from pyread_gadget_hdf5 import *
            from pyread_header_hdf5 import *
            import struct 
            import os
            import sys

    
     CALLING SEQUENCE:
            from hdf5totiamat import *
            Result = subfindtotiamat(filename [, longlong=True, noloop=True, silent=True, reslimit=True] )
    
     INPUTS:
           filename: Name of the file to read in. In case of a multiple files
                     output, any sub-file name can be given. The routine will
                     take care of reading data from each sub-file. Unless [noloop=True]

     OPTIONAL INPUTS:
    
     KEYWORD PARAMETERS (set to True)
           silent:      does not print any message on the screen
           longlong:    force int to be LongLong (code checks if needed however)
           noloop:      limit the read in to only the file selected (at this stage this is never triggered
                        as subfind has an issue with subdivided files that mean it doesn't share particles
                        and group properties correctly)
           reslimit:    Min number of DM particles in a group to be retained. 
                        If left as None, then sets to zero, if user selects True, defaults to 20, else takes user number passed. 
    
     OUTPUTS:
            A series of binary file in the '/data/' directory containing the SubFind output. Only using DM particles.
            If file is gas+DM then everything is recalulated (including resetting ParticleIDs) for the DM only.
    
     RESTRICTIONS:
    
    
     PROCEDURE:
    
    
     EXAMPLE:
        
            Convert the SubFind output to TIAMAT format:
            from hdf5togreg import *
            subfindtotiamat('/Users/aduffy/OWLS/REF_L025N128/data/subhalos_014/subhalo_014.0.hdf5')    
        
    
     MODIFICATION HISTORY (by Alan Duffy):
            
            1/10/13 Created the SubFind to TIAMAT conversion script

            Any issues please contact Alan Duffy on mail@alanrduffy.com or (preferred) twitter @astroduff
    """



#########################################################
##
## Define the Group Properties struct
##
#########################################################
    BINARY_TIAMAT_DATAFORMAT= OrderedDict(\
        [('id_MBP', uint64), ('M_vir', float64), ('n_particles', uint32), ('position_COM', (float32, 3)), 
        ('position_MBP', (float32, 3)), ('velocity_COM',(float32, 3)), ('velocity_MBP',(float32, 3)), 
        ('R_vir', float32), ('R_halo', float32), ('R_max', float32), ('V_max', float32), ('sigma_v', float32),
        ('spin', (float32, 3)), ('q_triaxial', float32), ('s_triaxial', float32), 
        ('shape_eigen_vectors', (float32, 9)), ('padding', uint8) ])
    BINARY_TIAMAT_DATATYPE = np.dtype({'names':BINARY_TIAMAT_DATAFORMAT.keys(), \
                                    'formats':BINARY_TIAMAT_DATAFORMAT.values()}, \
                                    align = True)

#########################################################
##
## Check user choices...
##
#########################################################
    mingr = 2
    if reslimit == None:
        reslimit = 1 # As I test for >= particles, and want to exclude zero.
    elif reslimit == True:
        reslimit = 20
    else:
        reslimit = reslimit

    if silent == None:
        print "GE Number of Particles for object is ", reslimit

    if silent == None:
        print "User gave "+filename

    ## Determine the file type, i.e. SubFind, Snapshot or old FOF.
    if filename.rfind('/subhalo') >= 0:
        inputtype = 'SubFind' ## Define File type
        inputname = 'subhalo'
        inputfolder = 'subhalos'
    elif filename.rfind('/group') >= 0:
        inputtype = 'FoF' ## Define File type
        inputname = 'group' 
        inputfolder = 'groups'
    elif filename.rfind('/snap') >= 0:
        inputtype = 'Snapshot' ## Define File type
        inputname = 'snap'
        inputfolder = 'snapshot'
    if silent == None:
        print "This is a "+inputtype+" output filetype"

    if inputtype != 'SubFind':
        print "This only works for SubFind output "
        return -1


#########################################################
##
## Determine if, and how many, subfiles we must loop over
##
#########################################################

    folder_index = filename.rfind(inputname)
    snapnum_index = filename[:folder_index-1].rfind('_')
    snapnum = int(filename[snapnum_index+1:folder_index-1])
    numfile = len(fnmatch.filter(os.listdir(filename[:folder_index]), '*.hdf5'))
    if noloop != None:
        numfileloop = 1 ## Force the code to only consider the input file
    else:
        numfileloop = numfile

    ## !! Can't consider SubFind split files alone, 
    ## !! particles in groups aren't stored on each as they're shared over subfiles.
    print "SubFind split files don't match particles present to the groups, can't subdivide "
    numfileloop = 1
    noloop = None

    folder_index = filename.rfind(inputfolder)
    for subfile in range(0,numfileloop):
        folder = filename[0:folder_index]
        if silent == None:
            print "Folder is "+folder
        infile = 'subhalos_'+str(snapnum).zfill(3)+'/subhalo_'+str(snapnum).zfill(3)+'.'+str(subfile)+'.hdf5'

        Header = pyread_header_hdf5(folder+infile, silent=silent)    

        ## Calculate Cosmology
        BoxSize = Header['BoxSize']
        DMpartMass = Header['MassTable'][1]
        Redshift = np.array( Header['Redshift'] )
        Omega0 = np.array( Header['Omega0'] )
        OmegaLambda = np.array( Header['OmegaLambda'] )
        OmegaBaryon = np.array( Header['OmegaBaryon'] )
        H_of_o = np.array( Header['HubbleParam']*100. ) ## Current Day Hubble Parameter 
        H_of_z = np.sqrt( H_of_o**2. * ( (Omega0 * (1.+Redshift)**3.)\
        + ( (1. - (Omega0+OmegaLambda)) * (1.+Redshift)**2.) + OmegaLambda)) ## Hubble Parameter at z
        scalefactor = np.array( 1. / (1. + Redshift) )
        sqrt_scalefactor = np.sqrt( scalefactor )

        if silent == None:
            print "At redshift ", Redshift, "or scalefactor ",scalefactor, " the Hubble Flow is ", H_of_z

        if OmegaBaryon < 0.001:
            if silent == None:
                print "No Baryons Present, DMONLY=True "
            dmonly = True
        else:
            if silent == None:
                print "Assuming Baryons Present, DMONLY=None "
            dmonly = None

        ## Calculate Units
        with h5py.File(folder+infile, "r") as fin:
            Utime = fin['Constants'].attrs['SEC_PER_YEAR']
            Umass = fin['Constants'].attrs['SOLAR_MASS']
            Ulength = fin['Constants'].attrs['CM_PER_MPC']
            CM_PER_KM = 1e5
            NewtonG = fin['Constants'].attrs['GRAVITY'] ## cgs [cm^3 g^-1 s^-2]
            NewtonG_kmssq_MpcMsol = NewtonG / (CM_PER_KM**2. * Ulength * Umass**-1.)
            NewtonG /= (Ulength**3. * Umass**-1. * Utime**-2.) ## Astro [Mpc^3 Msol^-1 yr^-2]
            if silent == None:
                print "Newton's Constant in [Mpc^3 Msol^-1 yr^-2] is ",NewtonG
                print "Newton's Constant in [(km/s)^2 Mpc Msol^-1] is ",NewtonG_kmssq_MpcMsol

#########################################################
##
## If the Simulation has baryons, need to remove the Baryon IDs
## can also check for total particle number, i.e. if Long Long INT
## is required
##
#########################################################
        if dmonly != None:
            BaryonFraction = 0.
            ConvFactor = np.array(1.)
        else:
            BaryonFraction = OmegaBaryon/Omega0
            ConvFactor = np.array(1./(1.-BaryonFraction))
        ## Need to calculate the number of baryon particles in the sim, and remove these.
        part_folder_index = filename.rfind('/data')
        part_index = filename[:part_folder_index-1].rfind('N')
        npart_name = (long(filename[part_index+1:part_folder_index]))**3
        ## Make a sanity check for the number calculated.
        MassInBox =BoxSize**3. * (3. * 100.**2)/(8.*np.pi*NewtonG_kmssq_MpcMsol)
        npart = MassInBox * (Omega0-OmegaBaryon)/ ( 1e10 * DMpartMass )
        if abs(npart/npart_name -1.) > 0.001:
            print "Something is wrong with the calculated particle numbers "
            print "The name suggests ", npart_name**(1./3.)
            print "But I calculate ", npart**(1./3.)
        if dmonly != None:
            npart_total = npart_name
        else:
            npart_total = 2.*npart_name        
            if silent == None:
                print "Must increase Particle Mass by ",ConvFactor

#########################################################
##
## Determine number of particles require Long Long INT
##
#########################################################
        if sum(Header['NumPart_Total'].astype(uint64)) < 1. and subfile == 0:
            if silent == None:
                print "Force counting due to SubFind bug"
            ## Bug in SubFind output, summate by hand
            for subloop in range(0,numfile):
                infile = 'subhalos_'+str(snapnum).zfill(3)+'/subhalo_'+str(snapnum).zfill(3)+'.'+str(subloop)+'.hdf5'
                Global_Header = pyread_header_hdf5(folder+infile, silent=silent)
                if subloop == 0:
                    Global_NumPart_Total_HighWord = Global_Header['NumPart_Total_HighWord']
                    Global_NumPart_Total = Global_Header['NumPart_ThisFile']
                else:
                    Global_NumPart_Total += Global_Header['NumPart_ThisFile']
                    Global_NumPart_Total_HighWord += Global_Header['NumPart_Total_HighWord']
            Header['NumPart_Total'] = Global_NumPart_Total
            Header['NumPart_Total_HighWord'] = Global_NumPart_Total_HighWord

        if sum(Header['NumPart_Total'].astype(uint64))+sum(Header['NumPart_Total_HighWord'].astype(uint64)) > 2.*1024.**3. or npart_total > 2.*1024.**3.:
            if silent == None:
                print "Long long int needed "
            intsz = uint64 ## Unsigned long long
            intstr = 'Q' ## Unsigned long long
            bytesz = 8
        else:
            if silent == None:
                print "Long int only "
            intsz = uint32
            intstr = 'I' ## Unsigned int
            bytesz = 4

        if longlong != None:
            if silent == None:
                print "Long long int needed "
            intsz = uint64
            intstr = 'Q' ## Unsigned long long
            bytesz = 8

        if silent == None:
            if noloop == None:
                print "Number of particles in file",Header['NumPart_Total'][1]
            else:
                print "Number of particles in file",Header['NumPart_ThisFile'][1]                

        NsubPerHalo=pyread_gadget_hdf5(folder+infile, 10, 'NsubPerHalo', noloop=noloop, sub_dir='SubFind', nopanda=True, silent=silent)
        if noloop != None:
            ToTNsubPerHalo=pyread_gadget_hdf5(folder+infile, 10, 'NsubPerHalo', sub_dir='SubFind', nopanda=True, silent=silent)
            ToTGrNr = 0 ## Track the position of the ToTNsubPerHalo array 
        
#########################################################
##
## Create the SubFind Group Catalogue File
##
#########################################################
        if silent == None:
            print "Now SubFind Group Info "

        out_directory = folder+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups/'
        if not os.path.exists(out_directory):
            if silent == None:
                print "Create Directory ", out_directory
            os.makedirs(out_directory)
        particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups.'+str(subfile)

        ## Read the SubFind Group Info 
        Header = pyread_header_hdf5(folder+infile, silent=silent,subheader=True)
        if noloop == None:                
            OrigNumGroups = Header['Total_Number_of_subgroups'].astype(intsz)
            if OrigNumGroups > mingr:
                tmpfile=pyread_gadget_hdf5(folder+infile, 1, 'SUB_Length', sub_dir='FoF', silent=silent).rename(columns={0: 'Length'})
        else:
            OrigNumGroups = Header['Number_of_subgroups'].astype(intsz)
            if OrigNumGroups > mingr:
                tmpfile=pyread_gadget_hdf5(folder+infile, 1, 'SUB_Length', noloop=noloop, sub_dir='FoF', silent=silent).rename(columns={0: 'Length'})

        if OrigNumGroups > mingr:
            gtreslim = tmpfile.Length >= reslimit
            NumGroups = len(tmpfile[gtreslim].Length)                 
            del(tmpfile)
        else:
            NumGroups = 0

        if noloop == None:
            TotNumGroups = NumGroups
        else:
            if NumGroups > mingr:
                tmpfile=pyread_gadget_hdf5(folder+infile, 1, 'SUB_Length', sub_dir='FoF', silent=silent).rename(columns={0: 'Length'})
                totalgtreslim = tmpfile.Length >= reslimit
                TotNumGroups = len(tmpfile[totalgtreslim].Length)
                del(tmpfile)
                del(totalgtreslim)
            else:
                TotNumGroups = 0

        ## Write the SubFind Catalog
        if silent == None:
            print "Writing to"+particles_outfile
        with open(particles_outfile, "wb") as fout:
            ## write 4-byte integer giving N_groups, the number of SubFind groups in the file
            fout.write(struct.pack('i', NumGroups))
            ## write 4-byte integer giving the number of bytes used for the group particle offsets (N_b_go)
            fout.write(struct.pack('i', bytesz))
            ## Write Body, 2-arrays, each N_b_go long

            ## write the Length array
            if NumGroups > mingr:
                dataset=pyread_gadget_hdf5(folder+infile, 1, 'SUB_Length', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent)
                fout.write(dataset[gtreslim].astype(uint32))
            else:
                fout.write(np.array([0]).astype(uint32))
            ## write the Offset array
            if NumGroups > mingr:
                dataset=pyread_gadget_hdf5(folder+infile, 1, 'SUB_Offset', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent)
                fout.write(dataset[gtreslim].astype(intsz)) 
            else:
                fout.write(np.array([0]).astype(uint32))

        if silent == None:
            print "Wrote SubFind Group file"

#########################################################
##
## Create the SubFind Subhalo Properties 
##
#########################################################

        if silent == None:
            print "Now SubFind Properties Info "
        out_directory = folder+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups_properties/'
        if not os.path.exists(out_directory):
            if silent == None:
                print "Create Directory ", out_directory
            os.makedirs(out_directory)

        particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups_properties.'+str(subfile)
        ## Read the SubFind Header
        Header = pyread_header_hdf5(folder+infile, silent=silent,subheader=True)

        ## Write the SubFind Catalog
        if silent == None:
            print "Writing to"+particles_outfile
        with open(particles_outfile, "wb") as fout:
            ## write 4-byte integer giving i_sub, the number of this file in the list of n_sub subfiles
            fout.write(struct.pack('i', subfile)) 
            ## write 4-byte integer giving n_sub, the number of subfiles in the directory
            fout.write(struct.pack('i', numfile)) 
            ## write 4-byte integer giving i_groups, the number of subhaloes in this file
            fout.write(struct.pack('i', NumGroups))
            ## write 4-byte integer giving n_groups, the number of subhaloes in all files
            fout.write(struct.pack('i', TotNumGroups))

#########################################################
##
## Have to calculate the SubFind Subhalo Properties 
##
#########################################################
            if TotNumGroups > mingr:

                ## Read in the Length and Offset arrays, crucial to figuring out which particles are in which FoF group
                dataset=pyread_gadget_hdf5(folder+infile, 1, 'SUB_Length', noloop=noloop, sub_dir='FoF', silent=silent).rename(columns={0: 'Length'})
                dataset['Offset']=pyread_gadget_hdf5(folder+infile, 1, 'SUB_Offset', noloop=noloop, sub_dir='FoF', silent=silent)

                ## Now read in the ParticleIDs
                particles=pyread_gadget_hdf5(folder+infile, 1, 'ParticleIDs', noloop=noloop, sub_dir='FoF', silent=silent).rename(columns={0: 'ParticleIDs'})
                if dmonly != None:
                    if silent == None:
                        print "There's only DM so don't change ParticleIDs"
                else:
                    if silent == None:
                        print "Reset the DM ParticleIDs, removing the gas count "
                    particles.ParticleIDs -= npart_name

                ## Coordinates are COMOVING [Mpc/h]
                particles=particles.join( \
                    pyread_gadget_hdf5(folder+infile, 1, 'Coordinates', noloop=noloop, sub_dir='FoF', silent=silent).\
                    rename(columns={0:'PosX', 1:'PosY', 2:'PosZ'}) )

                ## Velocites are PROPER [km/s]
                particles=particles.join( \
                    pyread_gadget_hdf5(\
                        folder+infile, 1, 'Velocity', noloop=noloop, sub_dir='FoF', silent=silent, physunits=True, leaveh=True).\
                    rename(columns={0:'VelX', 1:'VelY', 2:'VelZ'}) )

                ## Masses are 'proper' (of course) [Msol/h]  and correct for missing baryon particles (if neccessary)
                particles['Mass']=pyread_gadget_hdf5(\
                    folder+infile, 1, 'Mass', noloop=noloop, sub_dir='FoF', silent=silent, physunits=True, leaveh=True)
                if dmonly == None:
                    particles.Mass *= ConvFactor

                ## Establish SubFind GrNr, key to searching over particle info per group... a lot of particles not included in Subhaloes as unbound fluff (etc)
                ## So every particles given GrNr of -1 and only those in a group are given the non-negative number, which we search for, reassign entire df using this
                ## then have to recreate the Offfset array as this should be just a running total of Length
                particles['GrNr'] = np.empty(len(particles.ParticleIDs), dtype=dataset.Offset[0].dtype)
                particles.GrNr[:] = -1
                GrNrGroupNum = 0
                for GrNr in range(0,OrigNumGroups):
                    if dataset.Length[GrNr] >= reslimit:
                        particles.GrNr.ix[dataset.Offset[GrNr]:dataset.Offset[GrNr]+dataset.Length[GrNr]-1] = GrNrGroupNum
                        GrNrGroupNum += 1
                    else:
                        NsubPerHalo[GrNr] -= 1

                        if noloop != None:
                            ToTNsubPerHalo[ToTGrNr+GrNr] -= 1
                
                if noloop != None:
                    ToTGrNr += GrNr
                else:
                    ToTNsubPerHalo = NsubPerHalo

                NumGroups = GrNrGroupNum
                dataset = dataset[gtreslim].reset_index(0, drop=True)
                if len(dataset.Length) != NumGroups:
                    print "The number of subgroups selected ", NumGroups, " is different to the array size ",len(dataset.Length)
                ## All important GroupBy FoF Group Number

                particles = particles[particles['GrNr'] > -1].reset_index(drop=True)

                dataset.Offset = dataset.Length.cumsum()
                dataset.Offset = dataset.Offset.shift(1)
                dataset.Offset[0] = 0
                
                ## All important GroupBy Subhalo Group Number
                GrNrGroup = particles.groupby('GrNr', sort=False)

                ## Create Cumulative Mass of DM in each SubFind Subhalo
                particles['Vcirc'] = GrNrGroup['Mass'].apply(np.cumsum)

                ## Calculate radius, using get_rad function (calculateradius checks for periodicity of size BoxSize and recentres about Centre)
                get_rad = lambda GrNrGroup: calculateradius(GrNrGroup[['PosX','PosY', 'PosZ']], PeriodicBoxSize=BoxSize,\
                    Centre=[GrNrGroup[['PosX','PosY','PosZ']].reset_index(0, drop=True).ix[0]] )

                GrNrGroup = particles.groupby('GrNr', sort=False)

                particles = particles.join( \
                    pd.DataFrame( np.hstack( GrNrGroup.apply( get_rad ).values ) ).rename(columns={0: 'Radius'} ) )     

                # Radius convert to proper [Mpc/h]
                particles.Radius *= scalefactor

                ## sqrt(GM/R) proper [km/s]
                particles['Vcirc'] = particles.apply( lambda row: ( np.sqrt(NewtonG_kmssq_MpcMsol * row['Vcirc'] / row['Radius']) if row['Radius'] != 0. else 0.), axis=1)

                ## Should be Bryan & Norman (ApJ 495, 80, 1998) virial mass [M_sol/h], except for SubFind subgroups are just M_total (like below)
                binary_group = pd.DataFrame(dataset.Length * particles.Mass.ix[0]).rename(columns={'Length': 'M_vir'})

                ## ID of most bound DM particle in structure, first DM particle in the subgroup
                binary_group['id_MBP'] = particles.ParticleIDs[dataset.Offset].reset_index(0, drop=True)

                ## Number of particles in the structure
                binary_group['n_particles'] = dataset.Length.copy()

                ## MBP_Position Comoving Coordinates [Mpc/h]
                binary_group = binary_group.join(\
                    particles[['PosX','PosY','PosZ']].ix[dataset.Offset].reset_index(0, drop=True).\
                    rename(columns={'PosX': 'position_MBP_X','PosY': 'position_MBP_Y','PosZ': 'position_MBP_Z'}))

                ## MBP Velocity proper [km/s]
                binary_group = binary_group.join(\
                    particles[['VelX','VelY','VelZ']].ix[dataset.Offset].reset_index(0, drop=True).\
                    rename(columns={'VelX': 'velocity_MBP_X','VelY': 'velocity_MBP_Y','VelZ': 'velocity_MBP_Z'}))

                ## R_halo [Mpc/h] furthest particle from the Most Bound Particle, already converted to proper
                GrNrGroup = particles.groupby('GrNr', sort=False)
                binary_group['R_halo'] = GrNrGroup.Radius.apply( np.max )
                ## Virial Radius [Mpc/h]
                binary_group['R_vir'] = binary_group['R_halo'].copy()

                ## Get max(Vcirc) i.e. V_max reached at R_max (already in proper units)
                idx = GrNrGroup['Vcirc'].apply( np.argmax )
                binary_group['R_max'] = particles['Radius'][dataset.Offset+idx].values
                binary_group['V_max'] = particles['Vcirc'][dataset.Offset+idx].values

                ## Calculate the COM_Position (that's relative to the MBD position) checking for periodic boundary conditions
                ## then add back the MBD position in and we should have the true COM position, checking for periodic BC again. 
                ## units are comoving [Mpc/h]
                ## CoM Position comoving [Mpc/h] - Recalculate it with only the DM particles
                GrNrGroup = particles.groupby('GrNr', sort=False)
                for coord in ( 'X', 'Y', 'Z' ):
                    get_offset = lambda GrNrGroup: calculatediff(GrNrGroup['Pos'+coord], PeriodicBoxSize=BoxSize, Centre=[GrNrGroup['Pos'+coord].reset_index(0, drop=True).ix[0]] )
                    particles = particles.join( pd.DataFrame( np.hstack( GrNrGroup.apply( get_offset ).values ) ).rename(columns={0: 'Offset_'+coord} ) )
                    GrNrGroup = particles.groupby('GrNr', sort=False)
        
                    # Mass weighted Position, relative to the Most Bound Particle 
                    binary_group['position_COM_'+coord] = GrNrGroup.apply( lambda row: ( np.sum( row['Mass'] * row['Offset_'+coord], axis=1 ) / np.sum( row['Mass'] ) ) )
                    # Add the Most Bound Position Back to the COM position
                    binary_group['position_COM_'+coord] += binary_group['position_MBP_'+coord]
                    # Test for Periodic Boundaries (note that it's not half boxsize but full!)
                    binary_group['position_COM_'+coord][binary_group['position_COM_'+coord] < 0.] += BoxSize
                    binary_group['position_COM_'+coord][binary_group['position_COM_'+coord] > BoxSize] -= BoxSize
                    ## Get the mass weighted CoM Velocity, already proper [km/s]
                    binary_group['velocity_COM_'+coord] = GrNrGroup.apply( lambda row: ( np.sum( row['Mass'] * row['Vel'+coord], axis=1 ) / np.sum( row['Mass'] ) ) )

                    ## Make proper units
                    particles['Offset_'+coord] *= scalefactor
                ## Work out Sigma_v, the total 1D velocity dispersion, involves calculating the velocity difference per x/y/z direction
                ## relative to the Centre of Mass 'bulk' velocity, then adding in a Hubble Flow component (H(z) * radius from Centre of Mass)

                    ## Easiest way to do offset (unbelievably) appears to make EVERY particle share the velocity_COM_X value. Terrible waste of memory.
                    particles['VelOffset_'+coord] = np.zeros(len(particles.ParticleIDs), dtype=particles['Offset_'+coord].ix[0].dtype)
                    for GrNr in range(0,NumGroups):
                        particles['VelOffset_'+coord].ix[dataset.Offset[GrNr]:dataset.Offset[GrNr]+dataset.Length[GrNr]-1] = binary_group['velocity_COM_'+coord].ix[GrNr]

                    GrNrGroup = particles.groupby('GrNr', sort=False)
                    get_veloffset = lambda GrNrGroup: calculatediff(GrNrGroup['Vel'+coord], Centre=[GrNrGroup['VelOffset_'+coord].reset_index(0, drop=True).ix[0]] )

                    ## Work out the velocity offset value and add Hubble Flow
                    GrNrGroup = particles.groupby('GrNr', sort=False)
                    particles['VelOffset_'+coord] = pd.DataFrame( np.hstack( GrNrGroup.apply( get_veloffset ).values ) ) 
                    GrNrGroup = particles.groupby('GrNr', sort=False)

                    particles['VelOffset_'+coord] = particles.apply(lambda row: row['VelOffset_'+coord] + H_of_z * row['Offset_'+coord], axis=1)
                    GrNrGroup = particles.groupby('GrNr', sort=False)

                ## Add the particles velocity, relative to the CoM and with Hubble Flow, in quadrature with mass weighting
                binary_group['sigma_v'] = GrNrGroup.apply( lambda row: ( np.sqrt( \
                    (np.sum( row['Mass'] * row['VelOffset_X']**2., axis=1 ) + \
                     np.sum( row['Mass'] * row['VelOffset_Y']**2., axis=1 ) + \
                     np.sum( row['Mass'] * row['VelOffset_Z']**2., axis=1 ) ) \
                    / (3. * np.sum(row['Mass']) ) ) ) )

                ## Now work out spin, the specific angular momentum vector [Mpc/h * km/s]
                GrNrGroup = particles.groupby('GrNr', sort=False)
                binary_group['spin_X'] = GrNrGroup.apply( lambda row: (np.sum( row['Mass'] * (row['VelOffset_Z']*row['Offset_Y'] - row['VelOffset_Y']*row['Offset_Z']), axis=1 )\
                    / ( np.sum(row['Mass']) ) ))
                binary_group['spin_Y'] = GrNrGroup.apply( lambda row: (np.sum( row['Mass'] * (row['VelOffset_X']*row['Offset_Z'] - row['VelOffset_Z']*row['Offset_X']), axis=1 )\
                    / ( np.sum(row['Mass']) ) ))
                binary_group['spin_Z'] = GrNrGroup.apply( lambda row: (np.sum( row['Mass'] * (row['VelOffset_Y']*row['Offset_X'] - row['VelOffset_X']*row['Offset_Y']), axis=1 )\
                    / ( np.sum(row['Mass']) ) ))

                ## Just put a dummy variable here for triaxial shape, q, given as b/a
                binary_group['q_triaxial'] = np.zeros(NumGroups, dtype=binary_group.sigma_v.dtype)
                ## Just put a dummy variable here for triaxial shape, s, given as c/a
                binary_group['s_triaxial'] = np.zeros(NumGroups, dtype=binary_group.sigma_v.dtype)

                ## Close particles dataframe before loading anything more into memory
                del(particles)
                del(dataset)

                ## Manipulate the dataframe to fill the Tiamat struct and create the neccessary binary formatted output
                BINARY_TIAMAT_DATASET = np.empty(NumGroups, dtype=BINARY_TIAMAT_DATATYPE)
                for key, size in BINARY_TIAMAT_DATAFORMAT.iteritems():
                    if key == 'shape_eigen_vectors' or key == 'padding':
                        tmp = 1
                    else:
                        if len(BINARY_TIAMAT_DATASET[key].shape) > 1:
                            BINARY_TIAMAT_DATASET[key] = binary_group[[key+'_X', key+'_Y', key+'_Z']]#.astype(size)
                        else:
                            BINARY_TIAMAT_DATASET[key] = binary_group[key].astype(size)

                ## now output the struct
                BINARY_TIAMAT_DATASET.tofile(fout)

        if silent == None:
            print "Wrote SubFind Properties file"

#########################################################
##
## Create the FOF Group Catalogue File
##
#########################################################
        if silent == None:
            print "Now FOF Group Info "

        out_directory = folder+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups/'
        if not os.path.exists(out_directory):
            if silent == None:
                print "Create Directory ", out_directory
            os.makedirs(out_directory)
        particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups.'+str(subfile)

        ## Read the FOF Group Info 
        Header = pyread_header_hdf5(folder+infile, silent=silent,fofheader=True)

        ## Have to check if certain groups are under the resolution limit
        if noloop == None:                
            OrigNumGroups = Header['Total_Number_of_groups'].astype(intsz)
        else:
            OrigNumGroups = Header['Number_of_groups'].astype(intsz)

        if OrigNumGroups > mingr:
            tmpfile=pyread_gadget_hdf5(folder+infile, 1, 'Length', noloop=noloop, sub_dir='FoF', silent=silent).rename(columns={0: 'Length'})
            tmpfile['NsubPerHalo'] = NsubPerHalo
            gtreslim = (tmpfile.Length >= reslimit) & (tmpfile.NsubPerHalo > 0)            
            NumGroups = len(tmpfile[gtreslim].Length)   
            del(tmpfile)
        else:
            NumGroups = 0

        if noloop == None:
            ## Already read in the entire array
            TotNumGroups = NumGroups
        else:
            if NumGroups > mingr:
                tmpfile=pyread_gadget_hdf5(folder+infile, 1, 'Length', sub_dir='FoF', silent=silent).rename(columns={0: 'Length'})
                tmpfile['ToTNsubPerHalo'] = ToTNsubPerHalo
                totalgtreslim = (tmpfile.Length >= reslimit) & (tmpfile.ToTNsubPerHalo > 0)  
                TotNumGroups = len(tmpfile[totalgtreslim].Length)  
                del(tmpfile)
                del(totalgtreslim)
            else:
                TotNumGroups = 0

        ## Write the FOF Catalog
        if silent == None:
            print "Writing to"+particles_outfile
        with open(particles_outfile, "wb") as fout:
            ## write 4-byte integer giving N_groups, the number of FoF groups in the file
            fout.write(struct.pack('i', NumGroups))
            ## write 4-byte integer giving the number of bytes used for the group particle offsets (N_b_go)
            fout.write(struct.pack('i', bytesz))
            ## Write Body, 3-arrays, each N_b_go long

            ## write the Length array
            if NumGroups > mingr:
                dataset=pyread_gadget_hdf5(folder+infile, 1, 'Length', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent)
                fout.write(dataset[gtreslim].astype(uint32)) 
            else:
                fout.write(np.array([0]).astype(uint32))                 
            ## write the Offset array
            if NumGroups > mingr:
                dataset=pyread_gadget_hdf5(folder+infile, 1, 'Offset', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent)
                fout.write(dataset[gtreslim].astype(intsz)) 
            else:
                fout.write(np.array([0]).astype(uint32))                 

            ## write the SubGroup Number of Files
            if NumGroups > mingr:
                fout.write(NsubPerHalo[gtreslim].astype(uint32)) 
            else:
                fout.write(np.array([0]).astype(uint32))                 

        if silent == None:
            print "Wrote FOF Group file"


#########################################################
##
## Create the FOF Group Properties File
##
#########################################################
        if silent == None:
            print "Now FOF Properties Info "

        out_directory = folder+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups_properties/'
        if not os.path.exists(out_directory):
            if silent == None:
                print "Create Directory ", out_directory
            os.makedirs(out_directory)
        particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups_properties.'+str(subfile)

        ## Read the FOF Group Info 
        Header = pyread_header_hdf5(folder+infile, silent=silent,fofheader=True)

        ## Write the FOF Catalog
        if silent == None:
            print "Writing to"+particles_outfile
        with open(particles_outfile, "wb") as fout:
            ## write 4-byte integer giving i_sub, the number of this file in the list of n_sub subfiles
            fout.write(struct.pack('i', subfile)) 
            ## write 4-byte integer giving n_sub, the number of subfiles in the directory
            fout.write(struct.pack('i', numfile)) 
            ## write 4-byte integer giving i_groups, the number of FoF groups in this file
            fout.write(struct.pack('i', NumGroups))
            ## write 4-byte integer giving n_groups, the number of FoF groups in all files
            fout.write(struct.pack('i', TotNumGroups))

#########################################################
##
## Have to calculate the FOF Group Properties 
##
#########################################################
            if TotNumGroups > mingr:
                ## Read in the Length and Offset arrays, crucial to figuring out which particles are in which FoF group
                dataset=pyread_gadget_hdf5(folder+infile, 1, 'Length', noloop=noloop, sub_dir='FoF', silent=silent).rename(columns={0: 'Length'})
                dataset['Offset']=pyread_gadget_hdf5(folder+infile, 1, 'Offset', noloop=noloop, sub_dir='FoF', silent=silent)

                ## Now read in the ParticleIDs
                particles=pyread_gadget_hdf5(folder+infile, 1, 'ParticleIDs', noloop=noloop, sub_dir='FoF', silent=silent).rename(columns={0: 'ParticleIDs'})
                if dmonly != None:
                    if silent == None:
                        print "There's only DM so don't change ParticleIDs"
                else:
                    if silent == None:
                        print "Reset the DM ParticleIDs, removing the gas count "
                    particles.ParticleIDs -= npart_name

                ## Get positions in Mpc/h COMOVING
                particles=particles.join( \
                    pyread_gadget_hdf5(folder+infile, 1, 'Coordinates', noloop=noloop, sub_dir='FoF', silent=silent).\
                    rename(columns={0:'PosX', 1:'PosY', 2:'PosZ'}) )

                ## Get velocities in km/s proper
                particles=particles.join( \
                    pyread_gadget_hdf5(folder+infile, 1, 'Velocity', noloop=noloop, sub_dir='FoF', silent=silent, physunits=True, leaveh=True).\
                    rename(columns={0:'VelX', 1:'VelY', 2:'VelZ'}) )

                ## Get Mass in Msol/h and correct for missing baryon particles (if neccessary)
                particles['Mass']=pyread_gadget_hdf5(folder+infile, 1, 'Mass', noloop=noloop, sub_dir='FoF', silent=silent, physunits=True, leaveh=True)
                if dmonly == None:
                    particles.Mass *= ConvFactor

                ## Establish FoF GrNr, key to searching over particle info per group, set particles to -1 if not in group
                particles['GrNr'] = np.empty(len(particles.ParticleIDs), dtype=dataset.Offset[0].dtype)
                particles.GrNr[:] = -1
                GrNrGroupNum = 0
                for GrNr in range(0,OrigNumGroups):
                    if (dataset.Length[GrNr] >= reslimit) & (NsubPerHalo[GrNr] > 0):
                        particles.GrNr.ix[dataset.Offset[GrNr]:dataset.Offset[GrNr]+dataset.Length[GrNr]-1] = GrNrGroupNum
                        GrNrGroupNum += 1

                NumGroups = GrNrGroupNum
                dataset = dataset[gtreslim].reset_index(0, drop=True)

                if len(dataset.Length) != NumGroups:
                    print "The number of FoF groups selected ", NumGroups, " is different to the array size ",len(dataset.Length)            
                ## All important GroupBy FoF Group Number, none are likely removed for FoF. Just in case, recreate the Offset array.
                particles = particles[particles['GrNr'] > -1].reset_index(drop=True)
                dataset.Offset = dataset.Length.cumsum()
                dataset.Offset = dataset.Offset.shift(1)
                dataset.Offset[0] = 0

                GrNrGroup = particles.groupby('GrNr', sort=False)
                ## Create Cumulative Mass of DM in each FoF Group, call Vcirc as this will then be modifed
                particles['Vcirc'] = GrNrGroup['Mass'].apply(np.cumsum)

                ## Calculate radius, using get_rad function (calculateradius checks for periodicity of size BoxSize and recentres about Centre)
                get_rad = lambda GrNrGroup: calculateradius(GrNrGroup[['PosX','PosY', 'PosZ']], PeriodicBoxSize=BoxSize,\
                    Centre=[GrNrGroup[['PosX','PosY','PosZ']].reset_index(0, drop=True).ix[0]] )

                GrNrGroup = particles.groupby('GrNr', sort=False)
                particles = particles.join( pd.DataFrame( np.hstack( GrNrGroup.apply( get_rad ).values ) ).rename(columns={0: 'Radius'} ) )
                # Get radius into proper units, [Mpc/h]
                particles.Radius *= scalefactor

                ## sqrt(GM/R) naturally proper after I already converted Radius
                particles['Vcirc'] = particles.apply( lambda row: \
                    ( np.sqrt(NewtonG_kmssq_MpcMsol * row['Vcirc'] / row['Radius']) if row['Radius'] != 0. else 0.), axis=1)

                ## Bryan & Norman (ApJ 495, 80, 1998) virial mass 'proper' for what that's worth [M_sol/h]
                binary_group = pyread_gadget_hdf5(\
                    folder+infile, 10, 'Halo_M_TopHat200', noloop=noloop, sub_dir='SubFind', silent=silent, physunits=True, leaveh=True).\
                rename(columns={0: 'M_vir'})
                binary_group = binary_group[gtreslim].reset_index(0, drop=True)

                ## ID of most bound DM particle in structure
                binary_group['id_MBP'] = particles.ParticleIDs[dataset.Offset].reset_index(0, drop=True)

                ## Number of particles in the structure
                binary_group['n_particles'] = dataset.Length.copy()

                ## MBP_Position comoving [Mpc/h]
                binary_group = binary_group.join(\
                    particles[['PosX','PosY','PosZ']].ix[dataset.Offset].reset_index(0, drop=True).\
                    rename(columns={'PosX': 'position_MBP_X','PosY': 'position_MBP_Y','PosZ': 'position_MBP_Z'}))

                ## COM_Position comoving [Mpc/h]

                ## Calculate the COM_Position (that's relative to the MBD position) checking for periodic boundary conditions
                ## then add back the MBD position in and we should have the true COM position, checking for periodic BC again. 
                ## units are comoving [Mpc/h]
                ## CoM Position comoving [Mpc/h] - Recalculate it with only the DM particles
                GrNrGroup = particles.groupby('GrNr', sort=False)

                for coord in ( 'X', 'Y', 'Z' ):
                    get_offset = lambda GrNrGroup: calculatediff(GrNrGroup['Pos'+coord], PeriodicBoxSize=BoxSize, Centre=[GrNrGroup['Pos'+coord].reset_index(0, drop=True).ix[0]] )
                    particles = particles.join( pd.DataFrame( np.hstack( GrNrGroup.apply( get_offset ).values ) ).rename(columns={0: 'Offset_'+coord} ) )

                    GrNrGroup = particles.groupby('GrNr', sort=False)
                    # Mass weighted Position, relative to the Most Bound Particle 
                    binary_group['position_COM_'+coord] = GrNrGroup.apply( lambda row: ( np.sum( row['Mass'] * row['Offset_'+coord], axis=1 ) / np.sum( row['Mass'] ) ) )
                    # Add the Most Bound Position Back to the COM position
                    binary_group['position_COM_'+coord] += binary_group['position_MBP_'+coord]
                    # Test for Periodic Boundaries (note that it's not half boxsize but full!)
                    binary_group['position_COM_'+coord][binary_group['position_COM_'+coord] < 0.] += BoxSize
                    binary_group['position_COM_'+coord][binary_group['position_COM_'+coord] > BoxSize] -= BoxSize
                    ## Get the mass weighted CoM Velocity, already proper [km/s]
                    binary_group['velocity_COM_'+coord] = GrNrGroup.apply( lambda row: ( np.sum( row['Mass'] * row['Vel'+coord], axis=1 ) / np.sum( row['Mass'] ) ) )

                    ## Make proper units
                    particles['Offset_'+coord] *= scalefactor

                ## Work out Sigma_v, the total 1D velocity dispersion, involves calculating the velocity difference per x/y/z direction
                ## relative to the Centre of Mass 'bulk' velocity, then adding in a Hubble Flow component (H(z) * radius from Centre of Mass)

                    ## Easiest way to do offset (unbelievably) appears to make EVERY particle share the velocity_COM value. Terrible waste of memory.
                    particles['VelOffset_'+coord] = np.zeros(len(particles.ParticleIDs), dtype=particles['Offset_'+coord].ix[0].dtype)

                    for GrNr in range(0,NumGroups):
                        particles['VelOffset_'+coord].ix[dataset.Offset[GrNr]:dataset.Offset[GrNr]+dataset.Length[GrNr]-1] = binary_group['velocity_COM_'+coord].ix[GrNr]

                    GrNrGroup = particles.groupby('GrNr', sort=False)
                    get_veloffset = lambda GrNrGroup: calculatediff(GrNrGroup['Vel'+coord], Centre=[GrNrGroup['VelOffset_'+coord].reset_index(0, drop=True).ix[0]] )

                    ## Work out the velocity offset value and add Hubble Flow
                    particles['VelOffset_'+coord] = pd.DataFrame( np.hstack( GrNrGroup.apply( get_veloffset ).values ) ) 
                    GrNrGroup = particles.groupby('GrNr', sort=False)

                    particles['VelOffset_'+coord] = particles.apply(lambda row: row['VelOffset_'+coord] + H_of_z * row['Offset_'+coord], axis=1)

                GrNrGroup = particles.groupby('GrNr', sort=False)
                ## Add the particles velocity, relative to the CoM and with Hubble Flow, in quadrature with mass weighting
                binary_group['sigma_v'] = GrNrGroup.apply( lambda row: ( np.sqrt( \
                    (np.sum( row['Mass'] * row['VelOffset_X']**2., axis=1 ) + \
                     np.sum( row['Mass'] * row['VelOffset_Y']**2., axis=1 ) + \
                     np.sum( row['Mass'] * row['VelOffset_Z']**2., axis=1 ) ) \
                    / (3. * np.sum(row['Mass']) ) ) ) )

                ## MBP Velocity proper [km/s]
                binary_group = binary_group.join(\
                    particles[['VelX','VelY','VelZ']].ix[dataset.Offset].reset_index(0, drop=True).\
                    rename(columns={'VelX': 'velocity_MBP_X','VelY': 'velocity_MBP_Y','VelZ': 'velocity_MBP_Z'}))

                ## Virial Radius proper [Mpc/h]
                binary_group['R_vir'] = pyread_gadget_hdf5(\
                    folder+infile, 10, 'Halo_R_TopHat200', noloop=noloop, sub_dir='SubFind', silent=silent, physunits=True, leaveh=True)[gtreslim].reset_index(0, drop=True)
                ## R_halo [Mpc/h] furthest particle from the Most Bound Particle - in proper as Radius already converted
                GrNrGroup = particles.groupby('GrNr', sort=False)
                binary_group['R_halo'] = GrNrGroup.Radius.apply( np.max )
                GrNrGroup = particles.groupby('GrNr', sort=False)

                ## Get max(Vcirc) i.e. V_max (and R_max, where this happens) will already be proper as Radius was converted previously.
                idx = GrNrGroup['Vcirc'].apply( np.argmax )
                binary_group['R_max'] = particles['Radius'][dataset.Offset+idx].values
                binary_group['V_max'] = particles['Vcirc'][dataset.Offset+idx].values

                ## Now work out spin, the specific angular momentum vector [Mpc/h * km/s]
                GrNrGroup = particles.groupby('GrNr', sort=False)
                binary_group['spin_X'] = GrNrGroup.apply( lambda row: (np.sum( row['Mass'] * (row['VelOffset_Z']*row['Offset_Y'] - row['VelOffset_Y']*row['Offset_Z']), axis=1 )\
                    / ( np.sum(row['Mass']) ) ))
                binary_group['spin_Y'] = GrNrGroup.apply( lambda row: (np.sum( row['Mass'] * (row['VelOffset_X']*row['Offset_Z'] - row['VelOffset_Z']*row['Offset_X']), axis=1 )\
                    / ( np.sum(row['Mass']) ) ))
                binary_group['spin_Z'] = GrNrGroup.apply( lambda row: (np.sum( row['Mass'] * (row['VelOffset_Y']*row['Offset_X'] - row['VelOffset_X']*row['Offset_Y']), axis=1 )\
                    / ( np.sum(row['Mass']) ) ))

                ## Just put a dummy variable here for triaxial shape, q, given as b/a
                binary_group['q_triaxial'] = np.zeros(NumGroups, dtype=binary_group.sigma_v.dtype)
                ## Just put a dummy variable here for triaxial shape, s, given as c/a
                binary_group['s_triaxial'] = np.zeros(NumGroups, dtype=binary_group.sigma_v.dtype)

                ## Close particles dataframe before loading anything more into memory
                ParticleIDs = np.array(particles.ParticleIDs)
                del(particles)
                del(dataset)

                ## Manipulate the dataframe to fill the Tiamat struct and create the neccessary binary formatted output
                BINARY_TIAMAT_DATASET = np.empty(NumGroups, dtype=BINARY_TIAMAT_DATATYPE)
                for key, size in BINARY_TIAMAT_DATAFORMAT.iteritems():
                    if key == 'shape_eigen_vectors' or key == 'padding':
                        tmp = 1
                    else:
                        if len(BINARY_TIAMAT_DATASET[key].shape) > 1:
                            BINARY_TIAMAT_DATASET[key] = binary_group[[key+'_X', key+'_Y', key+'_Z']]#.astype(size)
                        else:
                            BINARY_TIAMAT_DATASET[key] = binary_group[key].astype(size)

                ## now output the struct
                BINARY_TIAMAT_DATASET.tofile(fout)
   
#########################################################
##
## Create the ParticleID Catalogue File
##
#########################################################

        out_directory = folder+'subfind_'+str(snapnum).zfill(3)+'.catalog_particles/'
        if not os.path.exists(out_directory):
            if silent == None:
                print "Create Directory ", out_directory
            os.makedirs(out_directory)
        particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_particles.'+str(subfile)

        ## Use the modified ParticleIDs created above, which has already deleted the Baryon IDs

        ## Write the Particle Catalog
        if silent == None:
            print "Writing to"+particles_outfile
        with open(particles_outfile, "wb") as fout:        
            ## write 4-byte integer that gives N_b, the byte-length of the particle IDs in the file
            fout.write(struct.pack('i', bytesz)) 
            if TotNumGroups > mingr:
                ## unsigned integer of length N_b giving N_p, the number of particles in the file
                fout.write(struct.pack('i', len(ParticleIDs)))
                ## write the Particle IDs
                fout.write(ParticleIDs.astype(intsz))
            else:
                ## unsigned integer of length N_b giving N_p, the number of particles in the file
                fout.write(struct.pack('i', 0))
                ## write the Particle IDs
                fout.write(np.array([0]).astype(intsz))
        if silent == None:
            print "Wrote Particle file"
