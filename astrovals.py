#!/usr/bin/env python
import numpy as np

#
# Convert from size - distance - angle for astronomy units of rad, deg, arc mins, arc s, m arc s
#

def convangle(angle, anglein = 'deg', angleout='arcm'):
	##angleunits can be deg, arcm, arcs, marcs

	## One degree in radians
	deg = np.pi / 180.
	radtodeg = 1./deg
	## Arcminutes, arcseconds, mill-arcsecons
	radtoarcm = 60.*radtodeg
	radtoarcs = 60.*radtoarcm
	radtomarcs = 1e3*radtoarcs

	angles = {'rad':1.,'deg':radtodeg,'arcm':radtoarcm,'arcs':radtoarcs,'marcs':radtomarcs}

	if anglein.lower() in angles.keys():
		angle /= angles[anglein]
	else:
		print "You specified ", anglein
		print "This isn't an option"

	if angleout.lower() in angles.keys():
		angle *= angles[angleout]
	else:
		print "You specified ", angleout
		print "This isn't an option"

	return angle


def getdist(angle, size, anglein = 'deg'):
	##angle units can be deg, arcm, arcs, marcs

	ang = convangle(angle, anglein = anglein, angleout='rad')

	return size / ang  

def getsize(angle, dist, anglein = 'deg'):
	##angle units can be deg, arcm, arcs, marcs

	ang = convangle(angle, anglein = anglein, angleout='rad')

	return dist * np.sin(ang)

def getangle(size, dist, angleout = 'deg'):
	##angle units out can be deg, arcm, arcs, marcs

	angle = np.arcsin(size / dist)
	ang = convangle(angle, anglein = 'rad', angleout=angleout)

	return ang
