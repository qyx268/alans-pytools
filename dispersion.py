##!/usr/bin/env ipython

import numpy as np
import pynbody as pn
#from scipy.interpolate import UnivariateSpline
import matplotlib.transforms as mtransforms
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.cm as cm

from mpl_toolkits.axes_grid1.parasite_axes import SubplotHost
#import cpickle as pickle

#filestem = '/Users/aduffy/Desktop/WTHERM_L010N0512/data/subhalos_103/subhalo_103'
#filestem = '/Users/aduffy/Test_NOSN_NOZCOOL_L010N0128/data/subhalos_103/subhalo_103'
#filestem = '/Volumes/Duffstore/PAISTE/WTHERM_L010N0512/data/subhalos_065/subhalo_065'
filestem = '/Users/aduffy/Test_NOSN_NOZCOOL_L010N0128/data/snapshot_103/snap_103.hdf5'

def plot_pdf(filein, outdir='', qty='rho_ne', presentation=False, plottoscreen=False, num_threads=4):
    """ Read Gadget3 hydro snapshot and output numpy file of qty as PDF """

    ## Stop interactive plots appearing
    if plottoscreen == False:
        plt.ioff()

    ## Set for speed up on GSTAR
    pn.config['number_of_threads'] = np.int(num_threads)

    #plt.rc('savefig',**{'bbox':'tight', 'dpi':500})
    if presentation != False:
        font = {'family':'monospace','sans-serif':['Chicago'],'weight':'bold', 'size':16}
    else:
        font = {'family':'monospace','sans-serif':['Chicago'],'weight':'bold', 'size':12}
    plt.rc('font',**font)
    
    if outdir:
        if outdir.endswith('/') == False:
            outdir += '/'
    else:
        outdir = filein.rsplit('/',1)
        outdir = outdir[0]+'/'

    fileout = outdir
    fileout += "PDF_"+qty+".pdf"


    ## Plotting options
    xtitle = r"Log10 "+qty+" (cm$^{-3}$)"
    ytitle = "PDF"
    legloc=1

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(xtitle)            
    ax.set_ylabel(ytitle)   

    if presentation == False:
        plt.xlabel(xtitle, fontsize=plt.rcParams['font.size']+6, weight=plt.rcParams['font.weight'])
        ax.tick_params(axis='x', labelsize=plt.rcParams['font.size']+3)
        plt.ylabel(ytitle, fontsize=plt.rcParams['font.size']+6, weight=plt.rcParams['font.weight'])
        ax.tick_params(axis='y', labelsize=plt.rcParams['font.size']+3)
    else:
        plt.xlabel(xtitle, fontsize=plt.rcParams['font.size'], weight=plt.rcParams['font.weight'])
        ax.tick_params(axis='x', labelsize=plt.rcParams['font.size'])
        plt.ylabel(ytitle, fontsize=plt.rcParams['font.size'], weight=plt.rcParams['font.weight'])
        ax.tick_params(axis='y', labelsize=plt.rcParams['font.size'])

    ## Load file
    tmpx = 100
    colors = cm.rainbow(np.linspace(0, 1, len(filein)))
    linelabel=filein
    for i, filetoread in enumerate(filein):
        s = pn.load(filetoread)
        s['pos'].convert_units('h**-1 Mpc a')
        boxsize = s.properties['boxsize']
        filesplit = filetoread.split('/')
        for wordval in filesplit:
            if '_L' in wordval:
                linelabel[i] = wordval
    
        ## Load qty
        values = s.g[qty]

        ## Take PDF of quantity
        binval, tmpx = np.histogram(np.log10(values.ravel()), bins=tmpx, density=True) # bin it into n bins
    
        xval = tmpx[:-1] + (tmpx[1] - tmpx[0])/2  

        ## Plot each in turn
        ax.plot(xval, binval, label=linelabel[i], color=colors[i])
    
    leg = ax.legend(loc=legloc)
    leg.get_frame().set_alpha(0) # this will make the box totally transparent
    leg.get_frame().set_edgecolor('white')
    for label in leg.get_texts():                
        label.set_fontsize('small') # the font size   
    for label in leg.get_lines():
        label.set_linewidth(4)  # the legend line width

    ## Output
    fig.tight_layout(pad=0.2)
    print "Plotting to ",fileout
    fig.savefig(fileout, dpi=fig.dpi*5)    

    if plottoscreen == True:
        plt.close()

def calcrender(filein, outdir='', presentation = False, res=False, zoom=False, dlos=False, ndlos=False, CalcVal=False, LoSVal=False, num_threads=4, plottoscreen=False):
    """ Read Gadget3 hydro snapshot and output numpy file of DM and SM map as a function of increasing line of sight distance """
    ## Stop interactive plots appearing
    if plottoscreen == False:
        plt.ioff()

    ## Set for speed up on GSTAR
    pn.config['number_of_threads'] = np.int(num_threads)

    plt.rc('savefig',**{'bbox':'tight', 'dpi':500})
    if presentation != False:
        font = {'family':'serif','serif':['Computer Modern'],'weight':'bold', 'size':16}
        #font = {'family':'sans-serif','serif':['Times New Roman'],'sans-serif':['Chicago'],'weight':'bold', 'size':16}
#        font = {'family':'monospace','sans-serif':['Chicago'],'weight':'bold', 'size':16}
    else:
        font = {'family':'sans-serif','serif':['Times New Roman'],'sans-serif':['Chicago'],'weight':'bold', 'size':12}
    plt.rc('text', **{'dvipnghack':True})
    plt.rc('font',**font)
    plt.rc('text', usetex=True)
    plt.rcParams['text.latex.preamble'] = [r'\boldmath']

    kwargs = {}#{'verbose':False}

    ## Pick a value, default will be set to DM
    if CalcVal == False:
        print "Warning: Have to pick a quantity of interest, otherwise default option is DM"
        CalcVal = 'DM'

    ## Do line of sight velocity for HA, redshift for DM, otherwise distance
    if LoSVal==False: 
        print "Warning: Have to pick a Line - of - sight distance / velocity or redshift to select along, otherwise default option:"
        if CalcVal == 'HA':
            print "H-alpha map will use velocity intervals as it is a spectral line "
            LoSVal='Diff_V' ## Could use Int_V ?
        elif CalcVal == 'DM':
            print "DM map will use redshift intervals as it is weighted by redshift "
            LoSVal='Int_Z' ## Could use Int_Z or Int_D
        else:
            print "Integrated map will use distance intervals as it is integrated along distance "
            LoSVal='Int_D'

    if outdir:
        if outdir.endswith('/') == False:
            outdir += '/'
    else:
        outdir = filein.rsplit('/',1)
        outdir = outdir[0]+'/'

    fileout = outdir

    s = pn.load(filein)
    s['pos'].convert_units('h**-1 Mpc a')
    boxsize = s.properties['boxsize']

    Hz = pn.analysis.cosmology.H(s)

    if (LoSVal=='Int_V') or (LoSVal=='Diff_V') :
        vboxsize = boxsize * Hz.in_units(' km s**-1 Mpc**-1' )
    ## Set the slice thickness along the line of sight in velocity space
        if (dlos != False) and (ndlos != False):
            if dlos*ndlos > vboxsize:
                print "Warning: User selected survey ", dlos*ndlos, " km/s along Line of Sight, setting dlos equal to velocity box width / ndlos"
                dlos = vboxsize / ndlos
            else:
                if abs(dlos*ndlos - vboxsize)/vboxsize > 1e-3: 
                    print "Warning: User selected survey ", dlos*ndlos, " km/s along Line of Sight missing out last ",vboxsize-dlos*ndlos, " km/s of box"
        elif (dlos == False) and (ndlos == False):
            print "Warning: User selected nothing for dlos thickness or number of vel steps along Line of Sight, use dlos=boxsize in velocity and ndlos=1."
            dlos = vboxsize/1.
            ndlos = 1.
        else:
            if dlos == False:
                dlos = vboxsize/ndlos
            elif ndlos == False:
                ndlos = vboxsize/dlos
            else:
                print "What did you select? ", dlos, ndlos
    elif (LoSVal=='Int_D') or (LoSVal=='Diff_D') :
    ## Set the slice thickness along the line of sight
        if (dlos != False) and (ndlos != False):
            if dlos*ndlos > boxsize:
                print "Warning: User selected survey ", dlos*ndlos, " Mpc along Line of Sight, setting dlos equal to boxsize / ndlos"
                dlos = boxsize / ndlos
            else:
                if abs(dlos*ndlos - boxsize)/boxsize > 1e-3: 
                    print "Warning: User selected survey ", dlos*ndlos, " Mpc along Line of Sight missing out last ",boxsize-dlos*ndlos, " Mpc of box"
        elif (dlos == False) and (ndlos == False):
            print "Warning: User selected nothing for dlos thickness or number of steps along Line of Sight, use dlos=boxsize and ndlos=1."
            dlos = boxsize/1.
            ndlos = 1.
        else:
            if dlos == False:
                dlos = boxsize/ndlos
            elif ndlos == False:
                ndlos = boxsize/dlos
            else:
                print "What did you select? ", dlos, ndlos
    elif (LoSVal=='Int_Z') or (LoSVal=='Diff_Z') :
    ## Set the slice thickness along the line of sight
        zboxsize = np.exp( boxsize * Hz.in_units(' km s**-1 Mpc**-1' ) / pn.units.c.in_units(' km s**-1 ') ) - 1.

        if (dlos != False) and (ndlos != False):
            if dlos*ndlos > zboxsize:
                print "Warning: User selected survey ", dlos*ndlos, " in redshift along Line of Sight, setting dlos equal to boxsize in redshift / ndlos"
                dlos = zboxsize / ndlos
            else:
                if abs(dlos*ndlos - zboxsize)/zboxsize > 1e-3: 
                    print "Warning: User selected survey ", dlos*ndlos, " in redshift along Line of Sight missing out last ",zboxsize-dlos*ndlos, " redshift of box"
        elif (dlos == False) and (ndlos == False):
            print "Warning: User selected nothing for dlos redshift thickness or number of steps along Line of Sight, use dlos=boxsize in redshift and ndlos=1."
            dlos = zboxsize/1.
            ndlos = 1.
        else:
            if dlos == False:
                dlos = zboxsize/ndlos
            elif ndlos == False:
                ndlos = zboxsize/dlos
            else:
                print "What did you select? ", dlos, ndlos

    ## Zoom into box
    if zoom:
        boxsize = boxsize / zoom

    ## Set the pixel resolution, convert from comoving kpc to npixel which is what pynbody uses
    if res:
        nres = np.int(boxsize/(res*1e-3))
    else:
        nres = np.int(500) ## Default
        res = boxsize*1e3/nres

    fileout += "Render_"+"{0:.3f}".format(res)+"kpc"

    if CalcVal == 'DM':
        qtylab = "Dispersion Measure [pc cm**-3]"
        minval = -2.
        maxval = 4.
        valstep = 0.001
        forcerange = np.array([-1.,3]) ## What I plot is more limited!
    elif CalcVal == 'SM':
        qtylab = "Scattering Measure [m**-17/3]"
        minval = 10.
        maxval = 20.
        valstep = 0.1
        forcerange = np.array([12.,17.]) ## What I plot is more limited!
    elif CalcVal == 'HA':
        qtylab = "L$_{Ha}$ [erg cm$^{-2}$ s$^{-1}$ sr$^{-1}$]"
        minval = -30.
        maxval = 0.
        valstep = 0.1
        forcerange = np.array([-22.,-10.]) ## What I plot is more limited!

    ## Set the bin ranges and arrays to store the histogram of the maps
    nvals = (maxval - minval) / valstep

    ValRange = minval+np.arange(int(nvals)+1)*valstep

    AllVal = np.empty(int(ndlos)*nvals).reshape(int(ndlos),nvals)
    AvAllVal = np.empty(int(ndlos)*nvals).reshape(int(ndlos),nvals)
    Dint = (np.arange(int(ndlos))+1)*dlos

    print "Set up LoS redshift / velocity units, warning assumes LoS is z direction!"
    ## Add the expansion velocity to the peculiar velocity
    pn.analysis.cosmology.add_hubble(s.g)
    ## Define the LoS velocity, in this case along the z direction, how can I automatically determine this?
    s.g['losvel'] = s.g['vel'][:,2]
    s.g['losdist'] = s.g['pos'][:,2]

    ## Filter out dense particles with n_h > 0.1 m_p cm^-3
    IGM = pn.filt.LowPass('rho','0.1 h**2. a**-3 m_p cm**-3')
 
    for i_zval, zval in enumerate(Dint):

        if 'Diff_' in LoSVal : ## It's a spectral line so only use short frequency / velocity intervals
            if i_zval == 0:
                if LoSVal == 'Diff_D':
                    print "Calculate for " + "{0:.2f}".format( 0. )+" - " + "{0:.2f}".format( Dint[i_zval] ) + " h**-1 Mpc a "
                    Interval = pn.filt.BandPass('losvel', "{0:.3f}".format( 0. )+' h**-1 Mpc a ', "{0:.3f}".format( Dint[i_zval] )+' h**-1 Mpc a ' ) 
                elif LoSVal == 'Diff_V':
                    print "Calculate for " + "{0:.2f}".format( 0. )+" - " + "{0:.2f}".format( Dint[i_zval] ) + " km s**-1 a**0.5 "                    
                    Interval = pn.filt.BandPass('losvel', "{0:.3f}".format( 0. )+' km s**-1 a**0.5 ', "{0:.3f}".format( Dint[i_zval] )+' km s**-1 a**0.5 ' ) 
                elif LoSVal == 'Diff_Z':
                    print "Calculate for dz = " + "{0:.2f}".format( 0. )+" - " + "{0:.2f}".format( Dint[i_zval] )                   
                    Interval = pn.filt.BandPass('redshift', "{0:.3f}".format( 0. ), "{0:.3f}".format( Dint[i_zval] ) ) 
            else:
                if LoSVal == 'Diff_D':
                    print "Calculate for " + "{0:.2f}".format( Dint[i_zval-1] )+" - " + "{0:.2f}".format( Dint[i_zval] ) + " h**-1 Mpc a "
                    Interval = pn.filt.BandPass('losvel', "{0:.3f}".format( Dint[i_zval-1] )+' h**-1 Mpc a ', "{0:.3f}".format( Dint[i_zval] )+' h**-1 Mpc a ' ) 
                elif LoSVal == 'Diff_V':
                    print "Calculate for " + "{0:.2f}".format( Dint[i_zval-1] )+" - " + "{0:.2f}".format( Dint[i_zval] ) + " km s**-1 a**0.5 "                    
                    Interval = pn.filt.BandPass('losvel', "{0:.3f}".format( Dint[i_zval-1] )+' km s**-1 a**0.5 ', "{0:.3f}".format( Dint[i_zval] )+' km s**-1 a**0.5 ' ) 
                elif LoSVal == 'Diff_Z':
                    print "Calculate for z_interval " + "{0:.2f}".format( Dint[i_zval-1] )+" - " + "{0:.2f}".format( Dint[i_zval] )                   
                    Interval = pn.filt.BandPass('redshift', "{0:.3f}".format( Dint[i_zval-1] ), "{0:.3f}".format( Dint[i_zval] ) ) 
        
        elif 'Int_' in LoSVal : ## It's a spectral line so only use short frequency / velocity intervals
            if LoSVal == 'Int_D':
                print "Calculate for " + "{0:.2f}".format( 0. )+" - " + "{0:.2f}".format( Dint[i_zval] ) + " h**-1 Mpc a "
                Interval = pn.filt.LowPass('losdist',"{0:.3f}".format( Dint[i_zval] )+' h**-1 Mpc a ')
            elif LoSVal == 'Int_V':
                print "Calculate for " + "{0:.2f}".format( 0. )+" - " + "{0:.2f}".format( Dint[i_zval] ) + " km s**-1 a**0.5 "
                Interval = pn.filt.LowPass('losvel',"{0:.3f}".format( Dint[i_zval] )+' km s**-1 a**0.5 ' )
            elif LoSVal == 'Int_Z':
                print "Calculate for z_interval " + "{0:.2f}".format( 0. )+" - " + "{0:.2f}".format( Dint[i_zval] )
                Interval = pn.filt.LowPass('losvel',"{0:.3f}".format( Dint[i_zval] ))

        if CalcVal == 'DM':
            imp = pn.plot.sph.image(s.g[Interval], qty='cosmodm', width=boxsize, units = "pc cm**-3", x1=0.,y1=0., qtytitle=qtylab, filename=fileout+"_DM"+"_zle"+"{:0>5d}".format(i_zval)+".png", vmin=10.**min(forcerange), vmax=10.**max(forcerange), resolution=nres, **kwargs)
        elif CalcVal == 'SM':
            imp = pn.plot.sph.image(s.g[Interval], qty='c_n_sq',width=boxsize, units = "m**-17/3", x1=0.,y1=0., qtytitle=qtylab, filename=fileout+"_SM"+"_zle"+"{:0>5d}".format(i_zval)+".png", vmin=10.**min(forcerange), vmax=10.**max(forcerange), resolution=nres, **kwargs)
        elif CalcVal == 'HA':
            imp = pn.plot.sph.image(s.g[IGM & Interval], qty='halpha',width=boxsize, units = "erg cm**-2 s**-1", x1=0.,y1=0., qtytitle=qtylab, filename=fileout+"_HA"+"_zle"+"{:0>5d}".format(i_zval)+".png", vmin=10.**min(forcerange), vmax=10.**max(forcerange), resolution=nres, **kwargs)

        ## Now create a PDF of those map values
        p, tmpx = np.histogram(np.log10(imp.ravel()), bins=ValRange, density=True) # bin it into n bins
        ## Store those values
        AllVal[i_zval,:] = p

        ## Take an average Value / D (this is < C_N^2 >) for LoSVel this per km/s
        p, tmpx = np.histogram(np.log10(imp.ravel()/zval), bins=ValRange, density=True) # bin it into n bins
        ## Store those values
        AvAllVal[i_zval,:] = p

    print "Saving file to ",fileout
    np.savez(fileout+'_'+CalcVal+'_'+LoSVal+'.npz', AllVal = AllVal, AvAllVal = AvAllVal, ValRange = ValRange, Dint = Dint)


def plotrender(filein, outdir='', presentation = False, CalcVal=False, LoSVal=False, plottoscreen=False):
    ## Stop interactive plots appearing
    if plottoscreen == False:
        plt.ioff()

    print "Reading in ",filein
    if outdir:
        if outdir.endswith('/') == False:
            outdir += '/'
    else:
        outdir = filein.rsplit('/',1)
        outdir = outdir[0]+'/'

    ytitle = "PDF"
    if presentation != False:
        plt.rc('font',**{'family':'monospace','sans-serif':['Chicago'],'weight':'bold', 'size':16})
        ## Have to set the labels by hand annoyingly
        plotargs = {'fontsize':plt.rcParams['font.size']+2, 'weight':plt.rcParams['font.weight']}
    else:
        plt.rc('font',**{'family':'monospace','sans-serif':['Chicago'],'weight':'bold', 'size':12})
        ## Have to set the labels by hand annoyingly
        plotargs = {'fontsize':plt.rcParams['font.size']+1, 'weight':plt.rcParams['font.weight']}

    ## Load the file
    if filein.endswith('.npz') == True:
        array = np.load(filein)
    else:
        array = np.load(filein+'.npz')

    if '_V' in LoSVal:
        avunit = ' V(km s**-1) '
        labelunit = ' km s**-1 '
    if '_Z' in LoSVal:
        avunit = ' dz '
        labelunit = avunit
    if '_D' in LoSVal:
        avunit = ' D(Mpc) '
        labelunit = ' h**-1 Mpc '
    Xrange = array['ValRange'] ## The range of the bins 
    Dint = array['Dint'] ## How far we integrated out too

    ## Set line colours
    colors = cm.rainbow(np.linspace(0, 1, len(Dint)))

    ## Set x plot range, convert bin edges to centers
    Xrange = Xrange[:-1] + (Xrange[1] - Xrange[0])/2  

    ## Loop over two plots, DM and <DM>
    for loop in (0,1):
        if loop == 0: 
            values = array['AllVal'] ## The binned PDF values
            if CalcVal == 'DM':
                xtitle = r"Log10(DM) (pc cm$^{-3}$)"
                fileout = outdir+'DM_hist.pdf'
                linelabel = r"DM for "
            elif CalcVal == 'SM':
                xtitle = r"Log10(SM) (m$^{-17/3}$)"
                fileout = outdir+'SM_hist.pdf'
                linelabel = r"SM for "
            elif CalcVal == 'HA':
                xtitle = r"Log10(HA) (erg cm$^{-2}$ s$^{-1}$)"
                fileout = outdir+'HA_hist.pdf'
                linelabel = r"HA for "
            legloc = 2
        elif loop == 1:
            values = array['AvAllVal'] ## The binned PDF values / averaged over LoS
            if CalcVal == 'DM':
                xtitle = r"Log10(<DM>) (pc cm$^{-3}$) / "+avunit
                fileout = outdir+'avDM_hist.pdf'
                linelabel = r"<DM> for "
            elif CalcVal == 'SM':
                xtitle = r"Log10(<SM>) (m$^{-17/3}$) / "+avunit
                fileout = outdir+'avSM_hist.pdf'
                linelabel = r"<SM> for "
            elif CalcVal == 'HA':
                xtitle = r"Log10(<HA>) (erg cm$^{-2}$ s$^{-1}$) / "+avunit
                fileout = outdir+'avHA_hist.pdf'
                linelabel = r"<HA> for "
            legloc = 1

        ## Now create a PDF of this
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_xlabel(xtitle)            
        ax.set_ylabel(ytitle)            
    #    ax.axis["top"].toggle(all=False)

        if presentation == False:
            plt.xlabel(xtitle, fontsize=plt.rcParams['font.size']+6, weight=plt.rcParams['font.weight'])
            ax.tick_params(axis='x', labelsize=plt.rcParams['font.size']+3)
            plt.ylabel(ytitle, fontsize=plt.rcParams['font.size']+6, weight=plt.rcParams['font.weight'])
            ax.tick_params(axis='y', labelsize=plt.rcParams['font.size']+3)
        else:
            plt.xlabel(xtitle, fontsize=plt.rcParams['font.size'], weight=plt.rcParams['font.weight'])
            ax.tick_params(axis='x', labelsize=plt.rcParams['font.size'])
            plt.ylabel(ytitle, fontsize=plt.rcParams['font.size'], weight=plt.rcParams['font.weight'])
            ax.tick_params(axis='y', labelsize=plt.rcParams['font.size'])

        ## Plot all ndlos objects tgogether 
        for i_zval,zval in enumerate(Dint):
            if 'Int_' in LoSVal:
                this_linelabel = linelabel + "{0:.2f}".format( Dint[0] )+" - " + "{0:.2f}".format( zval ) + labelunit
            elif 'Diff_' in LoSVal:
                if i_zval < len(Dint)-1:
                    this_linelabel = linelabel + "{0:.2f}".format( Dint[i_zval] )+" - " + "{0:.2f}".format( Dint[i_zval+1] ) + labelunit
                else:
                    this_linelabel = linelabel + "{0:.2f}".format( Dint[0] )+" - " + "{0:.2f}".format( Dint[i_zval] ) + labelunit
            if np.mod(i_zval, 10) == 0: ## Plot every tenth line
                ax.plot(Xrange, values[i_zval,:], label=this_linelabel, color=colors[i_zval])

        if len(Dint) < 100: ## Too many otherwise
            leg = ax.legend(loc=legloc)
            leg.get_frame().set_alpha(0) # this will make the box totally transparent
            leg.get_frame().set_edgecolor('white')
            for label in leg.get_texts():                
                label.set_fontsize('small') # the font size   
            for label in leg.get_lines():
                label.set_linewidth(4)  # the legend line width

        ## Output
        fig.tight_layout(pad=0.2)
        print "Plotting to ",fileout
        fig.savefig(fileout, dpi=fig.dpi*5)    

        if plottoscreen == True:
            plt.close()
