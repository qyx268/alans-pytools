#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Calculate Difference from X (can be scalar or array) from Centre (if desired; scalar or array) within a PeriodicBoxSize (if desired; scalar or array)."""

import numexpr as ne
import numpy as np

__author__ = 'Alan Duffy'
__email__ = 'mail@alanrduffy.com'
__version__ = '0.1.0'

def calculatediff(Position, Centre, PeriodicBoxSize=None):
    """ Calculate Difference from X (can be scalar or array) from Centre (if desired; scalar or array) within a PeriodicBoxSize (if desired; scalar or array)."""

    ## Force a numpy copy to be created for each array
    Position = np.array(Position).copy()
    Centre = np.array(Centre).copy()
    if PeriodicBoxSize != None:
        PeriodicBoxSize = np.array(PeriodicBoxSize).copy()
        HalfBoxSize = PeriodicBoxSize/2.
        NegHalfBoxSize = -1. * HalfBoxSize

    dim_p = len(Position.shape)
    dim_c = len(Centre.shape)

    # Check that the Position array is ge to the Centre array
    if dim_p >= dim_c:

        Position = ne.evaluate('Position - Centre')

        # Periodic Boundaries
        if PeriodicBoxSize != None:
            dim_b = len(PeriodicBoxSize.shape)
    # Check that the Position array is ge to the Periodic array
            if dim_p >= dim_b:
                Position = ne.evaluate('where(Position > HalfBoxSize, Position - PeriodicBoxSize, Position)')
                Position = ne.evaluate('where(Position < NegHalfBoxSize, Position + PeriodicBoxSize, Position)')
            else:
                print "Position is a smaller array than the PeriodicBoxSize provided, this can't work "
                return -2
    else:
        print "Position is a smaller array than the Centre provided, this can't work "
        return -1

    return Position
