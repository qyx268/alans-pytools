import numpy as np

def starburstimf(IMF = 'Chabrier'):
    """ Output the IMF mass intervals and power law slopes to use in Starburst99 for a given IMF

    Parameters
    ----------

    ion: Default is to output the Kroupa IMF, but Salpeter and Chabrier (an interpolated approximation to the low-mass exponential)

    Notes
    -----

    Requires numpy

    by
    Alan Duffy, 3/10/14 
    Queries to twitter... @astroduff  
    or email... mail@alanrduffy.com

    """

    if IMF.lower() == 'kroupa':
        powerlaw = np.array([1.3,2.3])
        massintervals = np.array([0.08,0.5,100.]) # 0.1 or 0.08 ? 
## Approximate with 0.1....
        massintervals = np.array([0.1,0.5,100.]) # 0.1 or 0.08 ? 

    elif IMF.lower() == 'salpeter':
        powerlaw = np.array([2.35])
        massintervals = np.array([0.1,100.]) # assume 0.1 as the minimum
    elif IMF.lower() == 'chabrier':
        ## Complex lower mass behaviour
        M = np.arange(0.08,1.,0.0001)
        A, Mc, sigma = [0.158, 0.079, 0.69] ## A units are [(log Msol)^-1 pc^-3] and Mc is Msol

        sigM = A * np.exp( -1.*(np.log10(M)-np.log10(Mc))**2./(2.*sigma**2.) )    ## sigM is sigma(log10(M))
        massintervals = np.linspace(0.08,1.,10, endpoint=True)
## Approximate with 0.1....
        massintervals = np.linspace(0.1,1.,5, endpoint=True)

        inds = np.digitize(M, massintervals)
        aggregate = [sigM[inds == i].mean() for i in np.unique(inds)]
        ## Has to be converted to alpha (essentially 1-x where 'x' is the power law slope approximation)
        powerlaw = 1. - np.log10(aggregate)
        powerlaw[-1] = 2.3 ## To agree with bright end slope
        ## Append the power law slope at high masses above the 1Msol
        massintervals = np.append(massintervals,[100.])

    ## Output to screen
    print "Number of IMF intervals "
    print powerlaw.size
    print "IMF Exponents "
    print ','.join([str(i) for i in powerlaw])
    print "Mass Boundaries for IMF "
    print ','.join([str(i) for i in massintervals])

    return